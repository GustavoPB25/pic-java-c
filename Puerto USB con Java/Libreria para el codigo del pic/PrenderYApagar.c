
#include <18F4550.h>
#fuses XTPLL,NOWDT,NOPROTECT,NOLVP,NODEBUG,USBDIV,PLL1,CPUDIV1,VREGEN
#use delay(clock=48000000)


#define USB_HID_DEVICE FALSE // deshabilitamos el uso de las directivas HID
#define USB_EP1_TX_ENABLE USB_ENABLE_BULK // turn on EP1(EndPoint1) for IN bulk/interrupt transfers
#define USB_EP1_RX_ENABLE USB_ENABLE_BULK // turn on EP1(EndPoint1) for OUT bulk/interrupt transfers
#define USB_EP1_TX_SIZE 32 // size to allocate for the tx endpoint 1 buffer
#define USB_EP1_RX_SIZE 32 // size to allocate for the rx endpoint 1 buffer


#include <pic18_usb.h> //  PIC18Fxx5x Hardware layer for CCS's PIC USB driver
#include "header.h" // Configuración del USB y los descriptores para este dispositivo
#include <usb.c> // handles usb setup tokens and get descriptor reports


const int8 Lenbuf = 32;
int8 recbuf[Lenbuf];


void main(void) {
  delay_ms(500);
  usb_init();
  usb_task();
  usb_wait_for_enumeration();
  enable_interrupts(global);
  while (TRUE){
    if(usb_enumerated()){
      if (usb_kbhit(1)){
        usb_get_packet(1, recbuf, Lenbuf);

        if(recbuf[0]==127){
           output_high(PIN_B0);
        }
        if(recbuf[0]==126){
          output_low(PIN_B0);
        }

      }
    }
  }
}
